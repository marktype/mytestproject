package com.alipay.share.demo.apshare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;
import com.alipay.share.demo.Constant;
import com.alipay.share.demo.MoreFuncActivity;
import com.alipay.share.demo.R;
import com.alipay.share.sdk.openapi.*;

import java.io.File;

public class ShareEntryActivity extends Activity implements View.OnClickListener, IAPAPIEventHandler {

    private IAPApi api;
    private static final String SDCARD_ROOT = Environment.getExternalStorageDirectory().getAbsolutePath();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        //使用appId创建一个IAPApi实例
        api = APAPIFactory.createZFBApi(getApplicationContext(), Constant.APP_ID, false);
        findViewById(R.id.text).setOnClickListener(this);
        findViewById(R.id.image).setOnClickListener(this);
        findViewById(R.id.webpage).setOnClickListener(this);
        findViewById(R.id.more_func).setOnClickListener(this);

        //处理分享结果回调信息
        if (getIntent() != null) {
            api.handleIntent(getIntent(), this);
        }
    }

    //处理分享结果回调信息
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text:
                sendTextMessage();
                break;
            case R.id.image:
                showImageShareDialog();
                break;
            case R.id.webpage:
                sendWebPageMessage();
                break;
            case R.id.more_func:
                Intent intent = new Intent(ShareEntryActivity.this, MoreFuncActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

    }

    /**
     * 分享文本信息
     */
    private void sendTextMessage() {
        String text = getString(R.string.share_text_default);

        //初始化一个APTextObject对象
        APTextObject textObject = new APTextObject();
        textObject.text = text;

        //用APTextObject对象初始化一个APMediaMessage对象
        APMediaMessage mediaMessage = new APMediaMessage();
        mediaMessage.mediaObject = textObject;

        //构造一个Req
        SendMessageToZFB.Req req = new SendMessageToZFB.Req();
        req.message = mediaMessage;

        //调用api接口发送消息到支付宝
        api.sendReq(req);

        finish();

    }

    /**
     * 分享网页消息
     */
    private void sendWebPageMessage() {
        //初始化一个APWebPageObject并填充网页链接地址
        APWebPageObject webPageObject = new APWebPageObject();
        webPageObject.webpageUrl = "https://open.alipay.com";

        //构造一个APMediaMessage对象，并填充APWebPageObject
        APMediaMessage webMessage = new APMediaMessage();
        webMessage.mediaObject = webPageObject;

        //填充网页分享必需参数，开发者需按照自己的数据进行填充
        webMessage.title = "分享网页标题";
        webMessage.description = "分享网页内容描述";
        webMessage.thumbUrl = "https://t.alipayobjects.com/images/rmsweb/T1vs0gXXhlXXXXXXXX.jpg";
        // 网页分享的缩略图也可以使用bitmap形式传输
        // webMessage.setThumbImage(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));

        //构造一个Req
        SendMessageToZFB.Req webReq = new SendMessageToZFB.Req();
        webReq.message = webMessage;
        webReq.transaction = buildTransaction("webpage");

        //调用api接口发送消息到支付宝
        api.sendReq(webReq);
        finish();

    }

    /**
     * 通过二进制流方式分享图片消息
     */
    private void sendByteImage() {
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);

        //初始化一个APImageObject
        APImageObject imageObject = new APImageObject(bmp);
        if (bmp != null)
            bmp.recycle();

        //构造一个APMediaMessage对象，并填充APImageObject
        APMediaMessage mediaMessage = new APMediaMessage();
        mediaMessage.mediaObject = imageObject;

        //构造一个Req
        SendMessageToZFB.Req req = new SendMessageToZFB.Req();
        req.message = mediaMessage;
        req.transaction = buildTransaction("image");

        //调用api接口发送消息到支付宝
        api.sendReq(req);
        finish();
    }

    /**
     * 通过图片链接方式分享图片消息
     */
    private void sendOnlineImage() {
        //图片Url，开发者需要根据自身数据替换该数据
        String url = "https://t.alipayobjects.com/images/rmsweb/T1vs0gXXhlXXXXXXXX.jpg";

        //初始化一个APImageObject
        APImageObject imageObject = new APImageObject();
        imageObject.imageUrl = url;

        //构造一个APMediaMessage对象，并填充APImageObject
        APMediaMessage mediaMessage = new APMediaMessage();
        mediaMessage.mediaObject = imageObject;

        //构造一个Req
        SendMessageToZFB.Req req = new SendMessageToZFB.Req();
        req.message = mediaMessage;
        req.transaction = buildTransaction("image");

        //调用api接口发送消息到支付宝
        api.sendReq(req);
        finish();

    }

    /**
     * 通过图片本地路径方式分享图片消息
     */
    private void sendLocalImage() {
        //图片本地路径，开发者需要根据自身数据替换该数据
        String path = SDCARD_ROOT + "/test.png";

        File file = new File(path);
        if (!file.exists()) {
            String tip = getString(R.string.img_file_not_exits);
            Toast.makeText(this, tip + " path = " + path, Toast.LENGTH_LONG).show();
            return;
        }

        //初始化一个APImageObject
        APImageObject imageObject = new APImageObject();
        imageObject.imagePath = path;

        //构造一个APMediaMessage对象，并填充APImageObject
        APMediaMessage mediaMessage = new APMediaMessage();
        mediaMessage.mediaObject = imageObject;

        //构造一个Req
        SendMessageToZFB.Req req = new SendMessageToZFB.Req();
        req.message = mediaMessage;
        req.transaction = buildTransaction("image");

        //调用api接口发送消息到支付宝
        api.sendReq(req);
        finish();
    }

    /**
     * 选择分享图片格式
     */
    private void showImageShareDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_share_image, null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(view).create();
        dialog.show();
        //图片本地路径分享
        view.findViewById(R.id.image_local_path).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendLocalImage();
            }
        });

        //图片二进制流分享
        view.findViewById(R.id.image_byte).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendByteImage();
            }
        });

        //图片url分享
        view.findViewById(R.id.image_url).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendOnlineImage();
            }
        });
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    /**
     * 分享结果回调信息处理
     */
    @Override
    public void onResp(BaseResp baseResp) {
        int result;

        switch (baseResp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                result = R.string.errcode_success;
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                result = R.string.errcode_cancel;
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                result = R.string.errcode_deny;
                break;
            case BaseResp.ErrCode.ERR_SENT_FAILED:
                result = R.string.errcode_send_fail;
                break;
            default:
                result = R.string.errcode_unknown;
                break;
        }
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
    }
}
