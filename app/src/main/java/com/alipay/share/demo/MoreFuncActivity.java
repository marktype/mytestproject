package com.alipay.share.demo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.alipay.share.sdk.openapi.APAPIFactory;
import com.alipay.share.sdk.openapi.IAPApi;

/**
 * Created by linghan.wj on 2015/7/14.
 */
public class MoreFuncActivity extends Activity implements View.OnClickListener {

    private IAPApi api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_more_func);
        findViewById(R.id.start_zfb).setOnClickListener(this);
        findViewById(R.id.check_version).setOnClickListener(this);
        //使用appId创建一个IAPApi实例
        api = APAPIFactory.createZFBApi(getApplicationContext(), Constant.APP_ID, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_zfb:
                api.openZFBApp();
                break;
            case R.id.check_version:
                Toast.makeText(this, "当前支持的sdk版本号为" + api.getZFBSupportAPI()
                        + "\n是否支持分享 " + api.isZFBSupportAPI(), Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

    }
}
